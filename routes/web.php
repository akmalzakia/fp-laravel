<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'PostController@index')->name('home');
Route::resource('post','PostController');
Route::resource('profile','ProfileController');
Route::resource('likepost','LikePostController')->only([
    'store','destroy'
]);
Route::resource('likecomment','LikeCommentController')->only([
    'store','destroy'
]);
Route::resource('post.comment','CommentController')->shallow();

Route::post('/follow','UserController@followUser')->name('follow');
Route::delete('/unfollow','UserController@unfollowUser')->name('unfollow');
