<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class replyComments extends Model
{
    //
    protected $guarded = [];

    public function comment(){
        return $this->belongsTo('App\postComments');
    }

    public function likes(){
        return $this->belongsToMany('App\User','reply_comment_likes');
    }

    public function user(){
        return $this->belongsTo('App\User');
    }
}
