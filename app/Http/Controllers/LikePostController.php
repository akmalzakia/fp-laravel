<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class LikePostController extends Controller
{

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
//        dd($request);
        $post = Post::find($request->id);
        $post->likes()->attach(Auth::user()->id);

        $count = $post->likes()->count();

        $post->update([
            'likes' => $count
        ]);

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        $post = Post::find($id);
        $post->likes()->detach(Auth::user()->id);

        $count = $post->likes()->count();

        $post->update([
            'likes' => $count
        ]);

        return back();
    }
}
