<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Traits\storeImageTrait;
use App\Profile;

class ProfileController extends Controller
{
    use storeImageTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('auth.account_setup');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
//        dd($request);
        $request->validate([
            'profile_image' => 'required',
            'display_name' => 'required',
            'bio' => 'required'
        ]);

//        if($request->hasFile('profile_image')){
////            dd($request);
//
//            $filenameWithExt =  $request->file('profile_image')->getClientOriginalName();
//            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
//            $extension = $request->file('profile_image')->getClientOriginalExtension();
//            $filenameFinal = $filename . '_' . time() . '.' . $extension;
//
//            $path = $request->file('profile_image')->storeAs('public/profile_images',$filenameFinal);
//        }
//        else{
//            $filenameFinal = 'noimage.jpg';
//        }

        $user = Auth::user();
        $profile = $user->profile()->create([
            'image' => $this->storeImageToPath($request,'profile_image', 'profile_images'),
            'display_name' => $request['display_name'],
            'bio' => $request['bio'],
            'followers' => 0,
            'following' => 0
        ]);

        return redirect('/post');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $profile = Profile::find($id);

        return view('profile', compact('profile'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $profile = Profile::find($id);
        return view('editProfile', compact('profile'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'display_name' => 'required',
            'bio' => 'required',

        ]);

        if($request->hasFile('image')){
            $profile_data = [
                'display_name' => $request->display_name,
                'bio' => $request->bio,
                'image' => $this->storeImageToPath($request,'image','profile_images')
            ];
        }
        else{
            $profile_data = [
                'display_name' => $request->display_name,
                'bio' => $request->bio,
            ];
        }


        $profile = Profile::find($id)->update($profile_data);
        return redirect()->route('profile.show', ['profile' => $id]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


}
