<?php

namespace App\Http\Controllers;

use App\postComments;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LikeCommentController extends Controller
{

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $comment = postComments::find($request->id);
        $comment->likes()->attach(Auth::user()->id);

        $count = $comment->likes()->count();

        $comment->update([
            'likes' => $count
        ]);

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $comment = postComments::find($id);
        $comment->likes()->detach(Auth::user()->id);

        $count = $comment->likes()->count();

        $comment->update([
            'likes' => $count
        ]);

        return back();
    }
}
