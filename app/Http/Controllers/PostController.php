<?php

namespace App\Http\Controllers;

use App\Post;
use App\postComments;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Traits\storeImageTrait;

class PostController extends Controller
{
    use storeImageTrait;

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        //
        $posts = Post::all();
        return view('post.post',compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('post.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        dd($request);
        //
        $request->validate([
            'judul' => 'required',
            'deskripsi' => 'required'
        ]);
        $user = Auth::user();
        $post = $user->post()->create([
            'judul' => $request['judul'],
            'deskripsi' => $request['deskripsi'],
            'image' => $this->storeImageToPath($request,'image','post_images'),
            'likes' => 0
        ]);

        return redirect('/post');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $post = Post::find($id);
        $comments = postComments::where('post_id',$id)->get();
        return view('post.show',compact('post','comments'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $post = Post::find($id);
        return view('post.edit',compact('post'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $request->validate([
            'judul' => 'required',
            'deskripsi' => 'required'
        ]);

        if($request->hasFile('image')){
            $query = Post::where('id',$id)->update([
                'judul' => $request['judul'],
                'deskripsi' => $request['deskripsi'],
                'image' => $this->storeImageToPath($request,'image','post_images')
            ]);
        }
        else{
            $query = Post::where('id',$id)->update([
                'judul' => $request['judul'],
                'deskripsi' => $request['deskripsi']
            ]);
        }

        return redirect('/post');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Post::destroy($id);
        return redirect('post');
    }
}
