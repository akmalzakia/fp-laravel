<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    //
    public function followUser(Request $request){
//        dd($request);
        $acc = User::find($request->id);

        Auth::user()->follow()->attach($acc);

        $follow = DB::table('user_follows')
            ->groupBy('followed_user_id')
            ->having('followed_user_id','=',$acc->id)
            ->count();

        $following = DB::table('user_follows')
            ->groupBy('user_id')
            ->having('user_id','=',Auth::user()->id)
            ->count();

        $acc->profile()->update([
            'followers' => $follow
        ]);

        Auth::user()->profile()->update([
            'following' => $following
        ]);

        return back();
    }

    public function unfollowUser(Request $request){
        $acc = User::find($request->id);


        Auth::user()->follow()->detach($acc);

        $follow = DB::table('user_follows')
            ->groupBy('followed_user_id')
            ->having('followed_user_id','=',$acc->id)
            ->count();

        $following = DB::table('user_follows')
            ->groupBy('user_id')
            ->having('user_id','=',Auth::user()->id)
            ->count();

        $acc->profile()->update([
            'followers' => $follow
        ]);

        Auth::user()->profile()->update([
            'following' => $following
        ]);

        return back();
    }
}
