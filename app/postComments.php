<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class postComments extends Model
{
    //
    protected $guarded = [];

    public function post(){
        return $this->belongsTo('App\Post');
    }

    public function reply(){
        return $this->hasMany('App\replyComments');
    }

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function likes(){
        return $this->belongsToMany('App\User','post_comment_likes','post_comment_id','user_id');
    }
}
