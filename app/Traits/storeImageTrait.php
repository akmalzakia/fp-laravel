<?php
namespace App\Traits;

use Illuminate\Http\Request;

trait storeImageTrait{


    public function storeImageToPath(Request $request,$key,$path){
        if($request->hasFile($key)){
            $filenameWithExt = $request->file($key)->getClientOriginalName();
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            $extension = $request->file($key)->getClientOriginalExtension();
            $filenameFinal = $filename . '_' . time() . '.' . $extension;

            $path = $request->file($key)->storeAs('public/' . $path,$filenameFinal);
        }
        else{
            $filenameFinal = 'noimage.jpg';
        }

        return $filenameFinal;
    }
}


?>
