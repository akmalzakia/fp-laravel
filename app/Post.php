<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    //
    protected $guarded = [];

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function comment(){
        return $this->hasMany('App\postComments');
    }

    public function likes(){
        return $this->belongsToMany('App\User','post_likes','post_id','user_id');
    }

}
