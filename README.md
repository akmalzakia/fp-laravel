# Final Project

## Kelompok 23

## Anggota Kelompok
1. Taqarra Rayhan Irfandianto
2. Akmal Zaki Asmara
3. Nizar Mayraldo

## Tema Project
Sosial Media

## ERD
![ERD](/public/erd.png "ERD")

## Link Video
Link Demo Aplikasi : https://drive.google.com/drive/folders/1D2-LGDnj6kXpoJxpR8IUuoCwd3IWyN12?usp=sharing

Link Deploy : https://young-sands-25636.herokuapp.com/
