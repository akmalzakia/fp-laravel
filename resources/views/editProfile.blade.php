@extends('adminlte.master')

@section('content')
    <div class="container">
        <div class="content-header">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <a class="btn btn-primary" href="{{route('profile.show',['profile' => $profile->id])}}">
                        <i class="fa fa-arrow-left"></i>
                        <span class="ml-2">Back</span>
                    </a>
                </div>
            </div>
        </div>
        <div class="card ">
            <div class="card-header">
                <h3 class="card-title">Edit Profile</h3>
            </div>
            <form action="{{ route('profile.update',['profile' => $profile->id])}}" method="POST" enctype="multipart/form-data">
                <div class="card-body">
                    @csrf
                    @method('put')

                    <div class="form-group row mt-1">
                        <label for="profile_image" class="col-md-2 col-form-label">{{ __('Profile Photo') }}</label>
                        <div class="col-md-6">
                            <input type="file" class="form-control-sm" id="profile_image" name="image" value="{{old('image','')}}" placeholder="Enter Image">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="display_name">Display Name</label>
                        <input type="text" class="form-control" name="display_name" id="display_name" value="{{ old('display_name',$profile->display_name) }}" placeholder="Masukkan Nama Lengkap">
                        @error('display_name')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="bio">Biodata</label>
                        <input type="text" class="form-control" name="bio" id="bio" value="{{ old('bio',$profile->bio) }}" placeholder="Masukkan Bio">
                        @error('bio')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>

                    <button type="submit" class="btn btn-primary">Edit Profile</button>

                </div>
            </form>
        </div>
    </div>
</div>
@endsection
