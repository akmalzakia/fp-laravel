@extends('adminLTE.master')

@section('content')
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <a class="btn btn-primary" href="{{route('post.index')}}">
                    <i class="fa fa-arrow-left"></i>
                    <span class="ml-2">Back</span>
                </a>
            </div>
            <div class="col-sm-6">
                @if(Auth::user()->id == $profile->user->id)
                    <a href="{{ route('profile.edit',['profile'=>Auth::user()->profile->id]) }}" class="btn fas fa-edit btn-outline-primary mb-3 float-sm-right">Edit Profile</a>
                @endif
            </div>
        </div>
    </div>
</div>
<div class="row d-flex justify-content-center">

    <div class="card "   style="width: 50%;">

        <div class="card-body ">
            <div class="image d-flex justify-content-center">
                <img src="{{asset('storage/profile_images/' . $profile->image)}}" class="img-circle elevation-1" width="300" height="300" alt="User Image">
            </div>
            <div class="">
                <h5 class=""><b>Name :</b></h5>
                <p class="card-text">{{ $profile->display_name }}</p>
            </div>
            <hr>
            <div class="">
                <h5 class=""><b>Biodata :</b></h5>
                <p class="card-text">{{ $profile->bio }}</p>
            </div>
            <hr>
            <div class="row my-2">
                <div class="col-md-6 text-center border-right">
                    <div>
                        <span class="font-weight-bold">Following</span>
                    </div>
                    <div>
                        <span class="text-primary font-weight-bold">{{$profile->following}}</span>
                    </div>
                </div>
                <div class="col-md-6 text-center">
                    <div>
                        <span class="font-weight-bold">Followers</span>
                    </div>
                    <div>
                        <span class="text-primary font-weight-bold">{{$profile->followers}}</span>
                    </div>
                </div>
            </div>
            @if(Auth::user()->id != $profile->user->id)


                    @if(Auth::user()->follow()->where('followed_user_id', $profile->user->id)->exists())
                        <form action="{{route('unfollow')}}" method="POST">
                            @csrf
                            @method('DELETE')
                            <hr>
                            <input type="hidden" name="id" value="{{$profile->user->id}}">
                            <div class="container-fluid">
                                <button type="submit" class="btn btn-danger btn-lg btn-block">Unfollow</button>
                            </div>
                        </form>
                    @else
                        <form action="{{route('follow')}}" method="POST">
                            @csrf
                            <hr>
                            <input type="hidden" name="id" value="{{$profile->user->id}}">
                            <div class="container-fluid">
                                <button type="submit" class="btn btn-primary btn-lg btn-block">Follow</button>
                            </div>
                        </form>
                    @endif

            @endif
        </div>
    </div>
</div>


<span style="display: inline;">

</span>

@endsection
