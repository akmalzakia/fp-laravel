@extends('adminLTE.master')

@section('content')
    <div class="container">
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <a class="btn btn-primary" href="{{route('post.show',['post'=>$post_id])}}">
                            <i class="fa fa-arrow-left"></i>
                            <span class="ml-2">Back</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    New Comment
                </div>
                <form action="{{route('post.comment.store')}}" method="POST">
                    @csrf
                    <div class="card-body">
                        <textarea class="form-control" rows="3" placeholder="Komentar" id="deskripsi" name="comment"></textarea>
                        <div class="form-group row mb-0">
                            <div class="container-fluid"><button type="submit" class="btn btn-primary float-right">
                                    {{ __('Post') }}
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
