@extends('adminLTE.master')

@section('content')
    <div class="container">
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <a class="btn btn-primary" href="{{route('post.index')}}">
                            <i class="fa fa-arrow-left"></i>
                            <span class="ml-2">Back</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    Edit Section
                </div>
                <form action="{{route('post.update',['post' => $post->id])}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <div class="card-body">
                        <div class="form-group row">
                            <label for="judulPost" class="col-sm-2 col-form-label">Judul</label>
                            <div class="col-sm-10">
                                @if($errors->has('judulPost'))
                                    <input type="text" class="form-control is-invalid" value="{{ old('judul',$post->judul) }}" id="judulPost" name="judul" placeholder="Judul">
                                    <span class="invalid-feedback">This field cannot be empty</span>
                                @else
                                    <input type="text" class="form-control" id="judulPost" value="{{ old('judul',$post->judul) }}" name="judul" placeholder="Judul">
                                @endif
                            </div>
                        </div>
                        <textarea class="form-control" rows="3" placeholder="What's on your mind?" id="deskripsi" name="deskripsi">{{ old('deskripsi',$post->deskripsi) }}</textarea>
                        <div class="form-group row mt-1">
                            <label for="profile_image" class="col-md-1 col-form-label">{{ __('Image') }}</label>
                            <div class="col-md-6">
                                <input type="file" class="form-control-sm" id="image" name="image" value="{{old('image',$post->image)}}" placeholder="Enter Image">
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="container-fluid"><button type="submit" class="btn btn-primary float-right">
                                    {{ __('Post') }}
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
