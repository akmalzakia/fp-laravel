@extends('adminLTE.master')

@section('content')
    <div class="container">
        <div class="content-header">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <a class="btn btn-primary" href="{{route('post.index')}}">
                        <i class="fa fa-arrow-left"></i>
                        <span class="ml-2">Back</span>
                    </a>
                </div>
{{--                <div class="col-sm-6">--}}
{{--                    <a class="btn btn-primary float-sm-right" href="#">--}}
{{--                        <i class="fa fa-plus"></i>--}}
{{--                        <span class="border-left m-2 p-2">--}}
{{--                            Add Comment--}}
{{--                        </span>--}}
{{--                    </a>--}}
{{--                </div>--}}
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-10">
                        <h2>{{$post->judul}}</h2>
                    </div>
                    <div class="col-sm-2">
                        @if($post->user->id == Auth::user()->id)
                            <div class="dropdown show float-right">
                                <a class="" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fa fa-ellipsis-v text-secondary"></i>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuLink">
                                    <a class="dropdown-item" href="{{route('post.edit',['post' => $post->id])}}">Edit</a>
                                    {{--                                        <a class="dropdown-item text-danger" href="#">Delete</a>--}}
                                    <form action="{{route('post.destroy',['post' => $post->id])}}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <input type="submit" value="Delete" class="dropdown-item text-danger">
                                    </form>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
                <hr>
                @if($post->image != "noimage.jpg")
                    <div class="container-fluid d-flex justify-content-center">
                        <img src="{{asset('storage/post_images/' . $post->image)}}" style="max-width: 500px; max-height: 500px; object-fit: cover">
                    </div>
                @endif
                <p>{{$post->deskripsi}}</p>
            </div>
            <div class="card-footer py-1">
                <div class="row" >
                    <div class="col-sm-10" style="border-radius: 5px 0px 0px 5px">
                        <div class="w-100">
                            <span style="font-size: 1.4em">
                                @if($post->likes()->where('user_id',Auth::user()->id)->exists())
                                    <form action="{{route('likepost.destroy',['likepost' => $post->id])}}" method='post'>
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-link btn-lg">
                                            <i class="fa fa-heart"></i>
                                            <span class="ml-1">{{$post->likes}}</span>
                                        </button>
                                    </form>
                                @else
                                    <form action="{{route('likepost.store')}}" method="POST">
                                        @csrf
                                        <input type="hidden" name="id" value="{{$post->id}}">
                                        <button type="submit" class="btn btn-link btn-lg">
                                            <i class="far fa-heart"></i>
                                            <span class="ml-1">{{$post->likes}}</span>
                                        </button>
                                    </form>

                                @endif
                            </span>
                        </div>
                        <div class="row w-100 pb-0 mb-0 mt-2">
                            <small class="col-sm-6 text-muted">
                                Created on : {{$post->created_at}}
                            </small>
                            <small class="col-sm-6 text-muted">
                                Last updated : {{$post->updated_at}}
                            </small>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <a href="{{route('profile.show',['profile' => $post->user->id])}}" class="border-left row" style="border-radius:0px 5px 5px 0px ">
                            <div class="col-sm-5">
                                <img src="{{asset('storage/profile_images/' . $post->user->profile->image)}}" class="rounded-circle my-2" style="object-fit: cover; width: 50px; height: 50px">
                            </div>
                            <div class="col-sm-7 align-items-center d-flex justify-content-center">
                                <span style="font-size: 0.9em">{{ $post->user->profile->display_name }}</span>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        @if(\Illuminate\Support\Facades\Route::currentRouteName() != 'comment.edit')
            <div class="card">
            <div class="card-body">
                <form action="{{route('post.comment.store',['post' => $post->id])}}" method="POST">
                    @csrf
                    <textarea class="form-control" rows="3" placeholder="Komentar" id="comment" name="text"></textarea>
                    <div class="container-fluid mt-2">
                        <button type="submit" class="btn btn-primary float-sm-right">
                            <i class="fa fa-plus"></i>
                            <span class="border-left m-2 p-2">
                                Add Comment
                            </span>
                        </button>
                    </div>
                </form>
            </div>
        </div>
            @forelse($comments as $key => $comment)
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-10">
                                <h2>Re : {{$post->judul}}</h2>
                            </div>
                            <div class="col-sm-2">
                                @if($comment->user->id == Auth::user()->id)
                                    <div class="dropdown show float-right">
                                        <a class="" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fa fa-ellipsis-v text-secondary"></i>
                                        </a>

                                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuLink">
                                            <a class="dropdown-item" href="{{route('comment.edit',['comment' => $comment->id])}}">Edit</a>
                                            {{--                                        <a class="dropdown-item text-danger" href="#">Delete</a>--}}
                                            <form action="{{route('comment.destroy',['comment' => $comment->id])}}" method="POST">
                                                @csrf
                                                @method('DELETE')
                                                <input type="submit" value="Delete" class="dropdown-item text-danger">
                                            </form>
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>
                        <hr>
                        <p>{{$comment->text}}</p>
                    </div>
                    <div class="card-footer py-1">
                        <div class="row" >
                            <div class="col-sm-10" style="border-radius: 5px 0px 0px 5px">
                                <div class="w-100">
                            <span style="font-size: 1.4em">
                                @if($comment->likes()->where('user_id',Auth::user()->id)->exists())
                                    <form action="{{route('likecomment.destroy',['likecomment' => $comment->id])}}" method='post'>
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-link btn-lg">
                                            <i class="fa fa-heart"></i>
                                            <span class="ml-1">{{$comment->likes}}</span>
                                        </button>
                                    </form>
                                @else
                                    <form action="{{route('likecomment.store')}}" method="POST">
                                        @csrf
                                        <input type="hidden" name="id" value="{{$comment->id}}">
                                        <button type="submit" class="btn btn-link btn-lg">
                                            <i class="far fa-heart"></i>
                                            <span class="ml-1">{{$comment->likes}}</span>
                                        </button>
                                    </form>

                                @endif
                            </span>
                                </div>
                                <div class="row w-100 pb-0 mb-0 mt-2">
                                    <small class="col-sm-6 text-muted">
                                        Created on : {{$comment->created_at}}
                                    </small>
                                    <small class="col-sm-6 text-muted">
                                        Last updated : {{$comment->updated_at}}
                                    </small>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <a href="{{route('profile.show',['profile' => $comment->user->id])}}" class="border-left row" style="border-radius:0px 5px 5px 0px ">
                                    <div class="col-sm-5">
                                        <img src="{{asset('storage/profile_images/' . $comment->user->profile->image)}}" class="rounded-circle my-2" style="object-fit: cover; width: 50px; height: 50px">
                                    </div>
                                    <div class="col-sm-7 align-items-center d-flex justify-content-center">
                                        <span style="font-size: 0.9em">{{ $comment->user->profile->display_name }}</span>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            @empty

            @endforelse
        @else
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-10">
                            <h2>Re : {{$post->judul}}</h2>
                        </div>
                    </div>
                    <hr>
                    <form action="{{route('comment.update',['comment' => $comment->id])}}" method="POST">
                        @csrf
                        @method('PUT')
                        <textarea class="form-control" rows="3" placeholder="Komentar" id="comment" name="text">{{old('text',$comment->text)}}</textarea>
                        <div class="container-fluid mt-2">
                            <button type="submit" class="btn btn-primary float-sm-right">
                                <i class="fa fa-edit"></i>
                                <span class="border-left m-2 p-2">
                                Edit Comment
                            </span>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        @endif
    </div>
@endsection
